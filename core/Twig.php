<?php

namespace Core;

use Core\Session\Session;
use GuzzleHttp\Psr7\Response;

class Twig
{
    private $twig;

    private $loader;

    public function __construct(Session $session = null)
    {
        $this->loader = new \Twig_Loader_Filesystem(dirname(__DIR__) . '/views');
        $this->twig = new \Twig_Environment($this->loader, []);

        if(!is_null($session)) {
            $this->addGlobal('session', $session);
        }
    }

    /**
     * Render a view template using Twig
     *
     * @param string $view  The template file
     * @param array $args  Associative array of data to display in the view (optional)
     *
     * @return string
     */
    public function render($view, $args = [])
    {
        return new Response(200, [], $this->twig->render($view, $args));
    }


    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function addGlobal(string $key, $value)
    {
        $this->twig->addGlobal($key, $value);
    }
}
