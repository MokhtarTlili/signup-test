<?php

namespace Core;

use GuzzleHttp\RedirectMiddleware;
use http\Env\Response;

class Controller
{
    /**
     * Redirect to a different page
     *
     * @param string $url  The relative URL
     *
     */
    public function redirect($url)
    {
        header('Location: '.$url, true, 301);
        exit;
    }
}
