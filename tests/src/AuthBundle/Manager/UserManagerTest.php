<?php

namespace Tests\App\AuthBundle\Manager;

use Core\DoctrineManager;
use App\AuthBundle\Entity\User;
use App\AuthBundle\Manager\UserManager;
use PHPUnit\Framework\TestCase;


class UserManagerTest extends TestCase
{
    /**
     * Test parseDataSignup function map the fiven data array to the user object
     */
    public function testParseDataSignup()
    {
        $manager = new UserManager();
        $user = new User();
        $expectedData = $this->getUserDataAsArray();
        $manager->parseDataSignup($user, $expectedData);

        $this->assertUserProperties($expectedData, $user);

    }

    /**
     * Test getResponseApiPayment function
     */
    public function testGetResponseApiPayment()
    {
        $manager = new UserManager();
        $user = new User();
        $expectedData = $this->getUserDataAsArray();
        $user->setFirstName($expectedData['firstname']);
        $user->setLastName($expectedData['lastname']);
        $user->setTelephone($expectedData['telephone']);
        $user->setAddress($expectedData['address']);
        $user->setHouseNumber($expectedData['house_number']);
        $user->setZip($expectedData['zip']);
        $user->setCity($expectedData['city']);
        $user->setAccountOwner($expectedData['account_owner']);
        $user->setIban($expectedData['iban']);

        $manager->save($user);

        $result = $manager->getResponseApiPayment($user);
        $this->assertInternalType('array', $result);
        $this->assertArrayHasKey('paymentDataId', $result);
    }

    /**
     * test function save should persist the user object
     */
    public function testSave()
    {
        $manager = new UserManager();
        $user = new User();
        $expectedData = $this->getUserDataAsArray();
        $user->setFirstName($expectedData['firstname']);
        $user->setLastName($expectedData['lastname']);
        $user->setTelephone($expectedData['telephone']);
        $user->setAddress($expectedData['address']);
        $user->setHouseNumber($expectedData['house_number']);
        $user->setZip($expectedData['zip']);
        $user->setCity($expectedData['city']);
        $user->setAccountOwner($expectedData['account_owner']);
        $user->setIban($expectedData['iban']);

        $manager->save($user);

        var_dump($user->getId());

        $em = DoctrineManager::entityManager();
        /** @var User $persistedUser */
        $persistedUser = $em->find(User::class, $user->getId());

        $this->assertUserProperties($expectedData, $persistedUser);
    }

    /**
     * @param array $expectedData
     * @param User $user
     */
    private function assertUserProperties(array $expectedData, User $user)
    {
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($expectedData['firstname'], $user->getFirstName());
        $this->assertEquals($expectedData['lastname'], $user->getLastName());
        $this->assertEquals($expectedData['telephone'], $user->getTelephone());
        $this->assertEquals($expectedData['address'], $user->getAddress());
        $this->assertEquals($expectedData['house_number'], $user->getHouseNumber());
        $this->assertEquals($expectedData['zip'], $user->getZip());
        $this->assertEquals($expectedData['city'], $user->getCity());
        $this->assertEquals($expectedData['account_owner'], $user->getAccountOwner());
        $this->assertEquals($expectedData['iban'], $user->getIban());
    }

    /**
     * This is like data provider
     * @return array
     */
    private function getUserDataAsArray()
    {
        return [
            'firstname' => 'mokhtar',
            'lastname' => 'tlili',
            'telephone' => '29081556',
            'address' => 'tunisie',
            'house_number' => '21',
            'zip' => '65656',
            'city' => 'souk lahad',
            'account_owner' => 'test',
            'iban' => 'DER654'
        ];
    }
}