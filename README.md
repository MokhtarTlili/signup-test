# This project is test for Wunder Compnay

## Steps installation :

### 1- clone this repository and move over to project folder :

`git clone https://MokhtarTlili@bitbucket.org/MokhtarTlili/signup-test.git`

### 2- Install packages run :

`composer install`

### 3- Create database called "test_signup", you can change it with parameters database in file :
core/Config/DB.php

### 4- Create Schema database run :

`./vendor/bin/doctrine orm:schema-tool:update --force`

### 5- Lanch the project run :

`php -S 127.0.0.1:8000 -t public`

### 6- Enjoy

=================================================================================================

# This project use MVC pattern and the PSR Recommendations such as :

* PSR11 = Dependency injection
* PSR7 = HTTP Request/Response
* PSR4 = Autoloader
* PSR2 = Style Code

=================================================================================================

# Packages :

* Twig Template engine
* Doctrine ORM
* Zend Expressive Fastroute system routing
* PHP-DI Dependency Injection
* Guzzle HTTP Request / Response

# Packages DEV :

* PHPUnit
* CodeSniffer
